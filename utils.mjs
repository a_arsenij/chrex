import {Browser, Builder, By} from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome.js";
import {parse} from "node-html-parser";

export async function sleep(ms) {
    return new Promise(res => {
        setTimeout(() => {res()}, ms);
    })
}

export function parseAmount(l) {
    if (l.endsWith("K")) {
        return parseFloat(l.slice(0, -1)) * 1000;
    }
    if (/\d+(\.\d+)?/.test(l)) {
        return parseFloat(l);
    }
    console.log("?? "+l);
}

export async function crawlUrl(url, limit) {
    const driver = await new Builder()
        .setChromeOptions((new chrome.Options()).addArguments('--headless=new'))
        .forBrowser(Browser.CHROME)
        .build();
    await driver.get(url);

    while (true) {
        const items = await driver.findElements(By.css("[data-item-id]"));
        if (items.length >= limit) {
            break;
        }
        let rem = 10;
        while (rem > 0) {
            await sleep(500);
            try {
                (await driver.findElement(By.xpath("//button[contains(., 'Load more')]"))).click();
                break;
            } catch (e) {
                rem--;
            }
        }
        if (rem === 0) {
            break;
        }
        await sleep(500);
    }
    const page = await driver.executeScript('return document.documentElement.innerHTML');
    const root = parse(page);
    const elems = root.querySelectorAll("[data-item-id]");
    const items = [];
    elems.forEach(e => {
        const id = e.getAttribute("data-item-id");
        const amount = parseAmount(e.querySelector(".Y30PE").textContent.slice(1, -1));
        items.push({
            id,
            amount,
        });
    });
    return items;
}
