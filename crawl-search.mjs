import {writeFileSync} from "fs";
import {crawlUrl} from "./utils.mjs";

const SEARCH_LIMIT = 410;

const QUERIES = process.argv.slice(2);

const FILENAME = "crawled_search.json";

const allItems = {};
let queryIndex = 0;
for (let query of QUERIES) {
    const url = "https://chromewebstore.google.com/search/" + query;
    console.log("Crawling " + url + " [" + Math.round(queryIndex * 100 / QUERIES.length) + "%]");
    const items = await crawlUrl(
        url,
        SEARCH_LIMIT
    );
    items.forEach(item => {
        allItems[item.id] = item;
    })
    queryIndex++;
}
writeFileSync(FILENAME, JSON.stringify(allItems));
console.log("Crawling search results completed, result dumped at " + FILENAME);
