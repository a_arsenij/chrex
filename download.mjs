

import { readFileSync, writeFileSync, existsSync } from "fs";

const TARGET_DIR = process.argv[2];
const JSONS = process.argv.slice(3);

async function download(
    id
) {
    const url = `https://clients2.google.com/service/update2/crx?response=redirect&prodversion=123.0.6312.105&acceptformat=crx2,crx3&x=id%3D${id}%26uc`;
    return fetch(url)
        .then(r => r.arrayBuffer())
        .then(b => {
            writeFileSync("/home/nameless/SSD2/exts/"+id, new Uint8Array(b));
            return b.byteLength;
        })
}

function sleep(ms) {
    return new Promise(res => {
        setTimeout(() => {res()}, ms);
    })
}

const items = {};
JSONS.forEach(file => {
    Object.values(
        JSON.parse(readFileSync(file, {encoding: "ascii"}))
    ).forEach(item => items[item.id] = item)
});
const list = Object.values(items).toSorted((a, b) => b.amount - a.amount);
let idx = 0;
const size = list.length;
for (let {id, amount} of list) {
    idx++;
    if (amount <= 0) {
        break;
    }
    if (!existsSync(TARGET_DIR+id)) {
        const l = await download(id);
        console.log("Downloaded "+id+" ("+amount+") "+l+"B "+idx+"/"+size)
        await sleep(10);
    } else {
        console.log("Skipping "+id+" (already downloaded)")
    }
}

console.log("Everything is downloaded at "+TARGET_DIR)
