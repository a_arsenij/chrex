import {writeFileSync} from "fs";
import {crawlUrl} from "./utils.mjs";

const CATEGORY_LIMIT = 1440;

const CATEGORIES = [
    "productivity/tools",
    "productivity/developer",
    "productivity/education",
    "productivity/communication",
    "productivity/workflow",
    "lifestyle/fun",
    "lifestyle/household",
    "lifestyle/well_being",
    "lifestyle/games",
    "lifestyle/art",
    "lifestyle/news",
    "lifestyle/shopping",
    "lifestyle/travel",
    "lifestyle/entertainment",
    "lifestyle/social",
    "make_chrome_yours/accessibility",
    "make_chrome_yours/functionality",
    "make_chrome_yours/privacy"
]

const FILTERS = [
    "",
    "filterBy=featured",
    "filterBy=establishedPublishers",
]

const SORTS = [
    "",
    "sortBy=highestRated"
]

const FILENAME = "crawled_categories.json";

const allItems = {};
let categoryIndex = 0;
for (let category of CATEGORIES) {
    let queryIndex = 0;
    queryParams: for (let filter of FILTERS) {
        for (let sort of SORTS) {
            let query = [filter, sort].filter(v => v).join("&");
            if (query.length > 0) {
                query = "?"+query;
            }
            const url = "https://chromewebstore.google.com/category/extensions/"+category+query;
            console.log("Crawling "+url+" ["+Math.round((categoryIndex + queryIndex / FILTERS.length / SORTS.length)*100/CATEGORIES.length)+"%]");
            const items = await crawlUrl(
                url,
                CATEGORY_LIMIT
            );
            items.forEach(item => {
                allItems[item.id] = item;
            })
            queryIndex++;
            if (items.length < CATEGORY_LIMIT) {
                console.log("Ignore other filters/sorts cuz category is too small")
                break queryParams;
            }
        }
    }
    categoryIndex++;
}
writeFileSync(FILENAME, JSON.stringify(allItems));
console.log("Crawling categories completed, result dumped at "+FILENAME);
