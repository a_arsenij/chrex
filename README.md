
Сначала:

`npm install`

Этапы:

### 1.1 Краулим список id экстеншнов из тех, что гугл выдаёт списком

`node crawl-list.mjs`

Примерно за час соберёт ~37к уникальных id экстеншнов. Выдаст в файл `crawled_categories.json`

### 1.2 Краулим список id экстеншнов по результатам поиска

`node crawl-search.mjs trading exchange stock markets rates crypto blockchain wallet bitcoin ethereum p2p`

Время работы сильно завист от количества кейвордов. Выдаст в файл `crawled_search.json`

### 2. Скачиваем экстеншны

```
node download.mjs /home/nameless/SSD2/exts/ crawled_categories.json crawled_search.json
                  ^ сюда подставляем папку, куда качаем экстеншны
                                            ^ перечисляем сколько угодно жсонов с айдишниками
```

В среднем качает ~1 экстеншн в секунду. Когда дойдёт до экстеншнов с <=5 отзывами, можно стопать, они нам не интересны.

Если экстеншн уже был скачан, повторно скачиваться не будет.

### 3. Распаковываем экстеншны

`bash unpack_non_unpacked.sh /home/nameless/SSD2/exts/ /home/nameless/SSD2/unpacked/`

Распакует те экстеншны, которые ещё не были распакованы. Будет ругаться `warning [...]: ... extra bytes at beginning or within zipfile`, игнорируем.

### 4. Грепаем

```bash
cd ПАПКА_КУДА_СКАЧИВАЛИ
sudo grep -lnr --include \*.js rulesType # по сомнительным причинам 
                                         # файлы в некоторых экстеншнах 
                                         # имеют странные права,
                                         # приходится грепать под судой
```
